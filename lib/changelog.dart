const contents = [
  // v1
  null,
  // v2
  null,
  // v3
  null,
  // v4
  null,
  // v5
  null,
  // v6
  null,
  // v7
  """1.7.0
Added HEIC support
Fixed a bug that corrupted the albums. Please re-add the photos after upgrading. Sorry for your inconvenience
""",
  // v8
  """1.8.0
Dark theme
""",
  // v9
  null,
  // v10
  null,
  // v11
  null,
  // v12
  null,
  // v13
  """13.0
Added MP4 support (Android only)
""",
  // v14
  null,
  // v15
  """15.0
This version includes changes that are not compatible with older versions. Please also update your other devices if applicable
""",
  // v16
  null,
  // v17
  """17.0
Archive photos to only show them in albums
Link to report issues in Settings
""",
  // v18
  """18.0
Modify date/time of photos
Support GIF
""",
];

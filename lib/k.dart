/// Version string shown in settings page
const versionStr = "18.1-9a7d20";
const version = 181;

/// Show a snack bar for a short amount of time
const snackBarDurationShort = const Duration(seconds: 4);

/// Show a snack bar for a normal amount of time
const snackBarDurationNormal = const Duration(seconds: 7);

/// Duration for short animation
const animationDurationShort = const Duration(milliseconds: 100);

/// Duration for normal animation
const animationDurationNormal = const Duration(milliseconds: 250);

/// Duration for long animation
const animationDurationLong = const Duration(milliseconds: 500);
